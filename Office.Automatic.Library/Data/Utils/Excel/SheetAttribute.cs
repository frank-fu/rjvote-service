﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office.Automatic.Library.Data.Utils.Excel
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class SheetAttribute : Attribute
    {
        public string Name { get; private set; }
        public bool HasHeadRow { get; set; } = false;
        public int HeadRowIndex { get; set; } = -1;
        public int DataRowIndex { get; set; } = -1;
        public int LastDataRowIndex { get; set; } = int.MaxValue;
        public SheetAttribute(string name)
        {
            Name = name;
        }
    }
}
