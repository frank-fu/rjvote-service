﻿namespace Vote.Project001.Core.Infrastructure
{
    public class DingApiException : Exception
    {
        public DingApiException(string message) : base(message)
        {

        }

        public DingApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
