﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace Office.Automatic.Library.Data.Utils.Excel
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true)]
    public class ColumnAttribute : Attribute
    {
        public int Index { get; set; } = -1;
        public string? Annotation { get; set; }
        public string? Name { get; set; }
    }
}
