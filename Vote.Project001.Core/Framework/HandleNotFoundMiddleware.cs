﻿namespace Vote.Project001.Core.Framework
{
    public class HandleNotFoundMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        public HandleNotFoundMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<HandleNotFoundMiddleware>();
        }
        public Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Response.StatusCode == 404)
            {
                _logger.LogCritical("Page not found:" + httpContext.Request.Path);
                
            }
            return _next(httpContext);
        }
    }

    public static class HandleNotFoundMiddlewareExtensions
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HandleNotFoundMiddleware>();
        }
    }
}
