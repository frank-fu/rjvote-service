﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office.Automatic.Library.Data.Utils.Excel
{
    public class ImportSetting
    {
        public DataConverter? Converter { get; set; }
        public bool LimitToHeadRange { get; set; } = true;

        internal DataConverter GetConverter()
        {
            if (Converter == null)
            {
                throw new InvalidOperationException();
            }
            return Converter;
        }
    }
}
