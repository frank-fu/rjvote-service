﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
#pragma warning disable CS8618

namespace Vote.Project001.Core.Database.Model
{
    [Table("Setting")]
    public class Setting
    {
        [Column(TypeName = "NVARCHAR")]
        [Required]
        [Key]
        public string Key { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Required]
        public string Value { get; set; }
    }
}
