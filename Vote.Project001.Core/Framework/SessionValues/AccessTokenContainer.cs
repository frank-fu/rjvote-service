﻿using AlibabaCloud.OpenApiClient.Models;
using AlibabaCloud.SDK.Dingtalkoauth2_1_0.Models;
using Vote.Project001.Core.Infrastructure;

namespace Vote.Project001.Core.Framework.SessionValues
{
    public class AccessTokenContainer: SessionValueContainer<string>
    {
        protected override TimeSpan Duration => new TimeSpan(0, 1, 50, 0);

        protected override string AcquireValue()
        {
            var client =
                new AlibabaCloud.SDK.Dingtalkoauth2_1_0.Client(
                    new Config() { Protocol = "https", RegionId = "central" });
            var response = client.GetAccessToken(new GetAccessTokenRequest()
            {
                AppKey = "dingqxznyklxtmhjn00p",
                AppSecret = "bNzN3xmJzE6M1PVT0tinaORUMTNIqJ0siW_x4FDmRGF71iZPCzLmuRgO6hqWXypI"
            });
            return response.Body.AccessToken;
        }
    }
}
