﻿namespace Vote.Project001.Core.Infrastructure
{
    public class WebApiException: Exception
    {
        public WebApiFailureEnum Failure { get; set; }

        public WebApiException(WebApiFailureEnum failure, params object[] args) : base(
            string.Format(WebApiFailureMessage.WebApiFailureMessageDictionary[failure], args))
        {
            Failure = failure;
        }
    }
}
