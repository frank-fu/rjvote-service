﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#pragma warning disable CS8618

namespace Vote.Project001.Core.Database.Model
{
    [Table("Vote")]
    [Index("UserId")]
    [Index("HeroId")]
    public class Vote
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity),Key, Column(TypeName = "INTEGER")]
        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Required]
        public string HeroId { get; set; }

        [Required]
        public DateTime VoteTime { get; set; }

        [Column(TypeName = "NVARCHAR")]
        public string UserId { get; set; }

        [Column(TypeName = "INTEGER")]
        public int CategoryId { get; set; }

    }
}
