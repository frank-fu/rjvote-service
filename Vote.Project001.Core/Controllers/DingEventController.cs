﻿using Microsoft.AspNetCore.Mvc;

namespace Vote.Project001.Core.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class DingEventController: ControllerBase
    {
        private const string AesKey = "dpMLNm7vsVNe9zjRaj4AAhizNjoM1ExkbmpjthDQaLH";
        private const string Token = "c2ksWb3PzYSMrilhEqXQj4jo9vfICTk4UQtj1mVdlRaSm5gc1s";
        private const string CropId = "dingin3oiwmybmj4hnih";
        public class Message
        {
            public string Encrypt { get; set; }
        }

        [HttpPost]
        public object PostMessage([FromQuery] string signature, [FromQuery] string timestamp, [FromQuery] string nonce,
            [FromBody] Message message)
        {
            var encryptor = new Infrastructure.DingTalkEncryptor(Token, AesKey, CropId);
            var msg = encryptor.GetDecryptMsg(signature, timestamp, nonce, message.Encrypt);
            Console.WriteLine("[DingEvent] Receive Msg: {0}", msg);

            return encryptor.GetEncryptedMap("success");
        }
    }
}
