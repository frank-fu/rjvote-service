﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Office.Automatic.Library.Data;
using Office.Automatic.Library.Data.Utils.Excel;

namespace Office.Automatic.Core.Jobs
{
    internal class Job20230310
    {
        [Sheet("Sheet1", HasHeadRow = true)]
        private class T01
        {
            [Column(Index = 0)]
            public int Id { get; set; }

            [Column(Name = "姓名")]
            public string Name { get; set; }

            [Column(Index = 2)] 
            public string BirthPlace { get; set; }

            [Column(Index = 3)]
            public string BirthTime { get; set; }

            [Column(Index = 4)]
            public string? PartyTime { get; set; }

            [Column(Index = 5)]
            public int IsParty { get; set; }

            [Column(Index = 6)]
            public string WorkTime { get; set; }

            [Column(Index = 7)]
            public string Duty { get; set; }

            [Column(Index = 8)]
            public int SWS { get; set; }

            [Column(Index = 9)]
            public int LS { get; set; }

            [Column(Index = 10)]
            public string? Awards { get; set; }
            

            [Column(Index = 12)]
            public string? Intro { get; set; }
        }

        [Serializable]
        class T02
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Department { get; set; }
            public string Introduction { get; set; }
            public string AvatarPaths { get; set; }
            public int CategoryId { get; set; }
        }

        public void Process()
        {
            var data = Importer.ImportFromExcel<T01>(@"C:\Users\loyse\Desktop\推荐表\三三机制钉钉投票-改.xlsx");

            var data02 = Newtonsoft.Json.JsonConvert.DeserializeObject<T02[]>(File.ReadAllText(
                @"C:\Users\loyse\source\repos\Office.Automatic.Core\Vote.Project001.Core\heroes.json",
                System.Text.Encoding.UTF8));

            var sw = new StreamWriter(@"C:\Users\loyse\Desktop\推荐表\a.txt");

            foreach (var t02 in data02)
            {
                foreach (var t01 in data)
                {
                    var id = $"hero-{t01.Id:D2}";
                    if (t02.Id != id)
                        continue;

                    var text = new StringBuilder();

                    text.Append($"　　{t01.Name}，");
                    text.Append($"{t01.BirthPlace}人，");
                    text.Append($"{t01.BirthTime.Replace(".0", ".").Replace('.', '年')}月出生，");
                    text.Append($"{t01.WorkTime.Replace(".0", ".").Replace(".", "年")}月进入税务系统工作，");
                    if (!string.IsNullOrEmpty(t01.PartyTime))
                    {
                        text.Append("中共党员，");
                    }
                    else if (t01.IsParty == 1)
                    {
                        text.Append("预备党员，");
                    }

                    if (t01.SWS == 1)
                        text.Append("税务师，");

                    if (t01.LS == 1) text.Append("律师从业资格，");

                    text.Append($"现任{t01.Duty}。");

                    if (!string.IsNullOrEmpty(t01.Intro))
                    {
                        text.Append(t01.Intro);
                    }

                    //if (!string.IsNullOrEmpty(t01.Awards))
                    //{
                    //text.Append("\n获得荣誉：\n　　" + t01.Awards.Replace("\n", "\n　　").Replace(";","；"));
                    //}
                    t02.Introduction = text.ToString();

                    sw.WriteLine(t01.Name);
                    sw.WriteLine(text.ToString());
                    sw.WriteLine();
                }

            }

            sw.Close();

            File.WriteAllText(@"C:\Users\loyse\source\repos\Office.Automatic.Core\Vote.Project001.Core\heroes.json",
                Newtonsoft.Json.JsonConvert.SerializeObject(data02, Formatting.Indented), System.Text.Encoding.UTF8);
        }
    }
}
