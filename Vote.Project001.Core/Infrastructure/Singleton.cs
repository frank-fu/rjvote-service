﻿namespace Vote.Project001.Core.Infrastructure
{
    public class Singleton<TClass> where TClass : Singleton<TClass>, new()
    {
        private static volatile TClass? _inst = null;
        protected static readonly object AccessLock = new();
        
        public static TClass Instance
        {
            get
            {
                if (_inst != null) return _inst;

                lock (AccessLock)
                {
                    if (_inst == null)
                    {
                        _inst = new TClass();
                        _inst.OnInitialize();
                    }
                }
                return _inst;
            }
        }

        public static TClass GetInstance()
        {
            return Instance;
        }

        protected virtual void OnInitialize()
        {

        }

    }
}
