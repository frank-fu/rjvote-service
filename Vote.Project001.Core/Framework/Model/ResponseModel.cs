﻿using Vote.Project001.Core.Infrastructure;

namespace Vote.Project001.Core.Framework.Model
{
    [Serializable]
    public class ResponseModel
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string? Message { get; set; }

        public ResponseModel(bool success, int errorCode, string? message = null)
        {
            Success = success;
            ErrorCode = errorCode;
            Message = message;
        }

        public static ResponseModel Failure(int errorCode, string? msg)
        {
            return new ResponseModel(false, errorCode, msg);
        }

        public static ResponseModel Failure(WebApiException exception)
        {
            return new ResponseModel(false, (int)exception.Failure, exception.Message);
        }

        public static ResponseModel Succeed()
        {
            return new ResponseModel(true, 0);
        }

        public static ResponseModel<TResult> Succeed<TResult>(TResult result)
        {
            return new ResponseModel<TResult>(true, 0, result);
        }
    }

    public class ResponseModel<TResult> : ResponseModel
    {
        public TResult Result { get; set; }

        public ResponseModel(bool success, int errorCode, TResult result, string? message = null) : base(success, errorCode, message)
        {
            Result = result;
        }
    }
}
