﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Vote.Project001.Core.Framework;
using Vote.Project001.Core.Framework.Model;

namespace Vote.Project001.Core.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class HeroController: ControllerBase
    {
        private IWebHostEnvironment _host;

        public HeroController(IWebHostEnvironment host)
        {
            _host = host;
        }

        [HttpGet()]
        public ResponseModel Reload(string code)
        {
            if (code != "Fujian@@@0215")
            {
                return ResponseModel.Failure(99,"Not allowed.");
            }

            if (_host.ContentRootFileProvider.GetFileInfo("heroes.json").Exists)
            {
                try
                {
                    HeroManager.Instance.Reload(Path.Combine(_host.ContentRootPath, "heroes.json"));
                    VoteManager.Instance.Reset();
                    return ResponseModel.Succeed();
                }
                catch (Exception ex)
                {
                    return ResponseModel.Failure(-1,
                        $"Database Failure: {ex.GetType().FullName}\n{ex.Message}\n{ex.StackTrace}");
                }
            }
            else
                return ResponseModel.Failure(1, "file not found");
        }

        [HttpGet()]
        public ResponseModel Update(string code)
        {
            if (code != "Fujian@@@0215")
            {
                return ResponseModel.Failure(99, "Not allowed.");
            }

            if (_host.ContentRootFileProvider.GetFileInfo("heroes.json").Exists)
            {
                try
                {
                    HeroManager.Instance.Update(Path.Combine(_host.ContentRootPath, "heroes.json"));
                    return ResponseModel.Succeed();
                }
                catch (Exception ex)
                {
                    return ResponseModel.Failure(-1,
                        $"Database Failure: {ex.GetType().FullName}\n{ex.Message}\n{ex.StackTrace}");
                }
            }
            else
                return ResponseModel.Failure(1, "file not found");
        }

        [HttpGet()]
        public ResponseModel<List<Framework.Model.Response.HeroInformation>> List(string sessionId)
        {
            return ResponseModel.Succeed(HeroManager.Instance.ListHeroWithVoteCount(sessionId));
        }

        [HttpGet()]
        public FileStreamResult GetAvatar(string heroId)
        {
            Console.WriteLine("[HeroController][Debug] Host->ContentRootPath: " + _host.ContentRootPath);
            var fileInfo = _host.ContentRootFileProvider.GetFileInfo($"wwwroot/static/avatar/{heroId}.jpg");
            Console.WriteLine("[HeroController][Debug] Request avatar path: " + _host.ContentRootPath +
                              $"wwwroot/static/avatar/{heroId}.jpg");
            if (fileInfo.Exists)
            {
                return File(fileInfo.CreateReadStream(), "image/jpeg");
            }
            else
            {
                throw new InvalidDataException("sd");
            }
        }
    }
}
