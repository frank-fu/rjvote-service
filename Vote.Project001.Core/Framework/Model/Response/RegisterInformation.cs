﻿#pragma warning disable CS8618
namespace Vote.Project001.Core.Framework.Model.Response
{
    public class RegisterInformation
    {
        public string Name { get; set; }
        public string SessionId { get; set; }
    }
}
