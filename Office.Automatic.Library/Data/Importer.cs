﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office.Automatic.Library.Data
{
    public static class Importer
    {
        public static TEntity[] ImportFromExcel<TEntity>(string excelFile, Utils.Excel.ImportSetting? setting = null)
            where TEntity : class, new()
        {
            return Utils.Excel.Importer.Import<TEntity>(excelFile, setting);
        }


    }
}
