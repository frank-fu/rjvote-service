using AlibabaCloud.OpenApiClient.Models;
using AlibabaCloud.SDK.Dingtalkoauth2_1_0.Models;
using Microsoft.AspNetCore.Mvc;
using Vote.Project001.Core.Database;
using Vote.Project001.Core.Framework;
using Vote.Project001.Core.Framework.Model;

namespace Vote.Project001.Core.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserController : ControllerBase
    {

        [HttpPost]
        public void SetFakeUserCode(string code)
        {
            SessionManager.Instance.SetFakeUserCode(code);
        }

        [HttpGet]
        public object GetFakeUserCode()
        {
            string code = SessionManager.Instance.GetFakeUserCode();
            return ResponseModel.Succeed(code);
        }

        [HttpPost()]
        public object RegisterUserCode([FromForm]string code)
        {
            try
            {
                var session = SessionManager.Instance.CreateSession(code);
                return ResponseModel.Succeed(new Framework.Model.Response.RegisterInformation()
                {
                    Name = session.Name,
                    SessionId = session.Id.ToString("N")
                });
            }
            catch (Exception ex)
            {
                return ResponseModel.Failure(-1, ex.Message);
            }

        }
    }
}