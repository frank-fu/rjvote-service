﻿/*
 * 用途：全国公务员管理信息系统(2024)版中个人简历添加全日制和在职学历
 *
 *
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace Office.Automatic.Core.Jobs
{

    public class EducationStatus 
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string? School { get; set; }
        public string? Major { get; set; }
        public string? Level { get; set; }

        public static bool operator>(EducationStatus a, EducationStatus b)
        {
            if (a.StartTime  != null && b.StartTime != null) {  return a.StartTime > b.StartTime; }
            else
            {
                return GetLevel(a.Level) > GetLevel(b.Level);
            }
        }

        public static bool operator <(EducationStatus a, EducationStatus b)
        {
            if (a.StartTime != null && b.StartTime != null) { return a.StartTime < b.StartTime; }
            else
            {
                return GetLevel(a.Level) < GetLevel(b.Level);
            }
        }

        private  static int GetLevel(string? value)
        {
            if (!string.IsNullOrEmpty(value)) return 0;

            var list=new List<string>() { "小学", "初中", "高中", "中专", "职高", "大专", "大学", "研究生" };

            for (var i = 0; i < list.Count; i++)
            {
                if (value.Contains(list[i]))
                    return i;
            }

            return 0;
        }
    }

    public class Resume
    {
        public string Id { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string ResumeText { get; set; } = string.Empty;
        public List<ResumeEntry> Entries { get; private set; } = new();
    }

    public struct ResumeEntry
    {
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Text { get; set; }
    }

    internal class Job20241206: Job
    {
        public override string Name => "Abc";
        public override bool IsTemporary => false;
        public override void Process()
        {
            var resumes = GetResumes(@"C:\Users\loyse\Desktop\Excel信息导入导出ForGatherV2024.01(国家税务总局仪征市税务局) (1).xlsx");
            ParseEduStatus(@"C:\Users\loyse\Desktop\yzxlzyxx.xlsx", out var eduStatusZZ, out var eduStatusQRZ);

            foreach (var resume in resumes)
            {
                List<EducationStatus>? esZZ = null, esQRZ = null;
                if (eduStatusZZ.ContainsKey(resume.Id)) esZZ = eduStatusZZ[resume.Id];
                if (eduStatusQRZ.ContainsKey(resume.Id)) esQRZ = eduStatusQRZ[resume.Id];

                esZZ?.Sort();
                esQRZ?.Sort();

                // Process QRZ
                var firstResumeTime = resume.Entries[0].StartTime;
                DateTime? lastEndTime = null;
                int index = 0;
                foreach (var es in esQRZ)
                {
                    if (es.StartTime != null && es.EndTime != null)
                    {
                        var e = new ResumeEntry()
                        {
                            StartTime = es.StartTime.Value,
                            EndTime = es.EndTime,
                            Text = es.School + (string.IsNullOrWhiteSpace(es.Major) ? "学生" : "专业学生")
                        };

                        if (lastEndTime != null && lastEndTime != es.StartTime)
                        {
                            var eDy = new ResumeEntry()
                            {
                                StartTime = es.StartTime.Value,
                                EndTime = es.EndTime,
                                Text = es.School + (string.IsNullOrWhiteSpace(es.Major) ? "学生" : "专业学生")
                            };
                        }

                        if (es.EndTime != null && es.EndTime < firstResumeTime)
                        {
                            resume.Entries.Insert(index, e);
                            lastEndTime = es.EndTime;
                            index++;
                        }
                    }
                }
            }

        }

        private List<Resume>? GetResumes(string file)
        {
            
            using var workbook = new XLWorkbook(file);

            var resumes = new List<Resume>();

            var sheet = workbook.Worksheet("A01人员");
            if (sheet == null) return null;

            var index = 3;
            while (true)
            {
                var nameCell = sheet.Cell(index, 1);
                if(nameCell== null) break;

                var name = nameCell.GetString();
                if(string.IsNullOrWhiteSpace(name)) break;

                var id = sheet.Cell(index, 3).GetString();
                var resumeText = sheet.Cell(index, 29).GetString();

                var resume = new Resume()
                {
                    Id = id,
                    Name = name,
                    ResumeText = resumeText
                };

                ParseResumeText(resume);

                resumes.Add(resume);
                index++;
            }



            return resumes;
        }

        private void ParseResumeText(Resume resume)
        {
            var regex = new Regex(
                "^([0-9]{4,4}.[0-9]{2,2})--(([0-9]{4,4}.[0-9]{2,2})|([ ]{7,7}))  ([\\w\\u4E00-\\u9FA5`~!@#$%^&*()_\\-+=<>?:\"{}|,.\\/;'\\\\[\\]·~！@#\uffe5%……&*（）——\\-+={}|《》？：“”【】、；‘'，。、]{1,})$",
                RegexOptions.Multiline);

            foreach (Match match in regex.Matches(resume.ResumeText))
            {
                var entry = new ResumeEntry()
                {
                    StartTime = new DateTime(int.Parse(match.Groups[1].Value.Substring(0, 4)),
                        int.Parse(match.Groups[1].Value.Substring(5, 2)), 1),
                    EndTime = string.IsNullOrWhiteSpace(match.Groups[2].Value)
                        ? null
                        : new DateTime(int.Parse(match.Groups[2].Value.Substring(0, 4)),
                            int.Parse(match.Groups[2].Value.Substring(5, 2)), 1),
                    Text = match.Groups[5].Value
                };

                resume.Entries.Add(entry);
            }
        }

        private void ParseEduStatus(string file ,out Dictionary<string, List<EducationStatus>> eduStatusZZ,
            out Dictionary<string, List<EducationStatus>> eduStatusQRZ)
        {
            eduStatusQRZ= new Dictionary<string, List<EducationStatus>>();
            eduStatusZZ = new Dictionary<string, List<EducationStatus>>();

            using var workbook = new XLWorkbook(file);

            var sheet=workbook.Worksheet("1");
            Debug.Assert(sheet != null);

            var index = 2;
            while (true)
            {
                var nameCell = sheet.Cell(index, 1);
                if (nameCell==null) break;

                var name = nameCell.GetString();
                if (string.IsNullOrWhiteSpace(name)) break;

                var id = sheet.Cell(index, 2).GetString();
                var type = sheet.Cell(index, 4).GetString();
                var startTimeT = sheet.Cell(index, 5)?.GetString();
                var endTimeT = sheet.Cell(index, 6)?.GetString();
                var school= sheet.Cell(index, 7)?.GetString();
                var major = sheet.Cell(index, 11)?.GetString();
                var level = sheet.Cell(index, 3)?.GetString();

                DateTime? st = string.IsNullOrWhiteSpace(startTimeT)
                    ? null
                    : new DateTime(int.Parse(startTimeT.Substring(0, 4)),
                        int.Parse(startTimeT.Substring(5, 2)), 1);

                DateTime? et = string.IsNullOrWhiteSpace(endTimeT)
                    ? null
                    : new DateTime(int.Parse(endTimeT.Substring(0, 4)),
                        int.Parse(endTimeT.Substring(5, 2)), 1);

                var es = new EducationStatus()
                {
                    Id = id,
                    Name = name,
                    School = school,
                    Major = major,
                    StartTime = st,
                    EndTime = et,
                    Level = level
                };


                if (type == "全日制教育")
                {
                    if (eduStatusQRZ.ContainsKey(id))
                    {
                        eduStatusQRZ[id].Add(es);
                    }
                    else
                    {
                        eduStatusQRZ.Add(es.Id, new List<EducationStatus> { es });
                    }
                }
                else
                {
                    if (eduStatusZZ.ContainsKey(id))
                    {
                        eduStatusZZ[id].Add(es);
                    }
                    else
                    {
                        eduStatusZZ.Add(es.Id, new List<EducationStatus> { es });
                    }
                }

                index++;
            }
        }
    }
}
