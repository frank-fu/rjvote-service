﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Spreadsheet;
using Office.Automatic.Library.Data;
using Office.Automatic.Library.Data.Utils.Excel;

namespace Office.Automatic.Core.Jobs
{
    internal class TempJob20230821 : Job
    {
        [Sheet("工作纪实表相关数据-2(简历)",HasHeadRow = true)]
        private class Resume
        {
            public string? 姓名 { get; set; }
            public string? 证件号码 { get; set; }
            public string? 出生日期 { get; set; }
            public string? 起始日期 { get; set; }
            public string? 终止日期 { get; set; }
            public string? 所在单位及部门 { get; set; }
            public string? 从事工作或担任职务 { get; set; }

        }

        [Sheet("Sheet3", HasHeadRow = true)]
        private class DataEntry
        {
            public string? 姓名 { get; set; }
            public string? 性别 { get; set; }
            public string? 民族 { get; set; }
            public string? 出生年月 { get; set; }
            public string? 籍贯 { get; set; }
            public string? 学历 { get; set; }
            public string? 工作时间 { get; set; }
            public string? 入党时间 { get; set; }
            public string? 单位及职务职级 { get; set; }
            public string? 任现职务层次时间 { get; set; }
            public string? 任现职级时间 { get; set; }
            public string? 任县以下职级时间 { get; set; }
            public string? 班子成员 { get; set; }
            public string? 其他科级 { get; set; }
            public string? 一般干部 { get; set; }
            public string? 合计 { get; set; }
            public string? 比例 { get; set; }
            public string? 排序 { get; set; }

        }

        public override string Name => "Temp";
        public override bool IsTemporary => true;

        public override void Process()
        {
            var data = Importer.ImportFromExcel<DataEntry>(@"C:\Users\loyse\Desktop\20230821\附件3：会议推荐汇总表.xlsx");
            var resumes = Importer.ImportFromExcel<Resume>(@"C:\Users\loyse\Desktop\20230821\工作纪实表相关数据-2(简历).xls");
            var workbook = new ClosedXML.Excel.XLWorkbook(@"C:\Users\loyse\Desktop\20230821\附件18：工作纪实表.xlsx");

            var index = 1;
            foreach (var entry in data)
            {
                if(entry.姓名==null) continue;

                var resume = GetResume(resumes.Where(t => t.姓名 == entry.姓名).OrderBy(t => t.起始日期));
                var sheet = workbook.Worksheet("个人部分").CopyTo($"{index:D2}.{entry.姓名}");
                sheet.Cell("C3").SetValue(entry.姓名);
                sheet.Cell("E3").SetValue(entry.性别);
                sheet.Cell("G3").SetValue(ParseDate(entry.出生年月));
                sheet.Cell("I3").SetValue(entry.籍贯);
                if (!string.IsNullOrEmpty(entry.入党时间))
                {
                    sheet.Cell("E4").SetValue("中共党员");
                    sheet.Cell("G4").SetValue(ParseDate(entry.入党时间));
                }
                else
                {
                    sheet.Cell("E4").SetValue("群众");
                }

                sheet.Cell("I4").SetValue(entry.学历);
                sheet.Cell("C5").SetValue($"国家税务总局仪征市税务局{entry.单位及职务职级}");
                sheet.Cell("E5").SetValue(ParseDate(entry.任现职务层次时间));
                sheet.Cell("G5").SetValue(ParseDate(entry.任现职级时间)); 
                sheet.Cell("I5").SetValue(ParseDate(entry.任县以下职级时间));
                sheet.Cell("C6").SetValue(resume);


                sheet.Cell("G9").SetValue(Convert.ToInt32(entry.合计));
                sheet.Cell("H9").SetValue(entry.比例);
                sheet.Cell("I9").SetValue(Convert.ToInt32(entry.排序));
                sheet.Cell("C11").SetValue(Convert.ToInt32(entry.排序));

                index++;
                Console.WriteLine("{0} handled.", entry.姓名);
            }

            workbook.Save();
        }

        private string ParseDate(string? date)
        {
            if (string.IsNullOrEmpty(date)) return "0000.00";
            return $"{date.Substring(0, 4)}.{date.Substring(4, 2)}";
        }
        private string GetResume(IEnumerable<Resume> resumes)
        {
            var sb = new StringBuilder();
            foreach (var resume in resumes)
            {
                if(string.IsNullOrEmpty(resume.起始日期)) continue;

                sb.AppendLine(
                    string.Format("{0}--{1}  {2}{3}",
                        resume.起始日期.Substring(0, 7).Replace('-', '.'),
                        string.IsNullOrEmpty(resume.终止日期) ? "       " : resume.终止日期.Substring(0, 7).Replace('-', '.'),
                        resume.所在单位及部门, 
                        resume.从事工作或担任职务));

            }

            return sb.ToString(0, sb.Length - 2);
        }
    }
}
