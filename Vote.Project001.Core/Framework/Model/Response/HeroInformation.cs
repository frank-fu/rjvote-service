﻿namespace Vote.Project001.Core.Framework.Model.Response
{
    public class HeroInformation
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Department { get; set; }

        public string AvatarPaths { get; set; }

        public string Introduction { get; set; }

        public int CategoryId { get; set; }

        public int VoteCount { get; set; }

        public bool Voted { get; set; }
    }
}
