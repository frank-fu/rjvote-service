﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office.Automatic.Core.Jobs
{
    internal abstract class Job
    {
        public abstract string Name { get; }
        public abstract bool IsTemporary { get; }
        public abstract void Process();
    }
}
