﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
#pragma warning disable CS8618

namespace Vote.Project001.Core.Database.Model
{
    [Table("Hero")]
    public class Hero
    {
        [Column(TypeName = "NVARCHAR")]
        [Key]
        public string Id { get; set; }


        [Column(TypeName = "NVARCHAR")]
        [Required]
        public string Name { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Required]
        public string Department { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Required]
        public string AvatarPaths { get; set; }


        [Column(TypeName = "NVARCHAR")]
        [Required]
        public string Introduction { get; set; }

        [Column(TypeName = "INTEGER")]
        [Required]
        public int CategoryId { get; set; }

        public List<Vote> Votes { get; set; }
    }
}
