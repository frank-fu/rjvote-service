﻿using Microsoft.AspNetCore.Mvc;
using Vote.Project001.Core.Framework;
using Vote.Project001.Core.Framework.Model;
using Vote.Project001.Core.Infrastructure;

namespace Vote.Project001.Core.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class VoteController: ControllerBase
    {

        [HttpPost]
        public ResponseModel Vote([FromForm] string sessionId, [FromForm] string heroId)
        {
            try
            {
                VoteManager.Instance.Vote(sessionId, heroId);
                return ResponseModel.Succeed();
            }
            catch (WebApiException ex)
            {
                return ResponseModel.Failure(ex);
            }
            catch(Exception ex)
            {
                return ResponseModel.Failure(-1, ex.Message);
            }

        }

        [HttpPost]
        public ResponseModel<Dictionary<string, int>> Sync()
        {
            return ResponseModel.Succeed(VoteManager.Instance.GetVoteData());
        }
    }
}
