﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office.Automatic.Library.Data.Utils.Excel
{
    public enum CellDataType
    {
        Numeric = 0,
        String = 1,
        Boolean = 4,
        Null=999,
    }
}
