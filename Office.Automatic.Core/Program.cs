﻿using DocumentFormat.OpenXml.Spreadsheet;
using System.IO.MemoryMappedFiles;
using System.Text;
using System.Text.RegularExpressions;
using static NPOI.HSSF.Util.HSSFColor;

namespace Office.Automatic.Core
{
    internal class Program
    {

        static void Main(string[] args)
        { 
            new Jobs.Job20241206().Process();
           //TempFunc();
        }


        static void TempFunc()
        {

            var content = File.ReadAllText("C:\\Users\\loyse\\Desktop\\题目.txt", System.Text.Encoding.UTF8);

            var regex = new Regex(@"^([0-9]{1,3})\.(\S+\r\n(?:[ABCDEFG]\.\S+\r\n){0,})(?:\r\n){0,2}(?:答案：([ABCDEFG对错]+))*(?:\r\n){0,2}(解析：(\S+))*\r\n",
                RegexOptions.Multiline);

            

            var matches = regex.Matches(content);

            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            StringBuilder sb3 = new StringBuilder();
            int[] p = new int[]
            {
                6, 17, 19, 20, 21, 23, 24, 27, 28, 33, 38, 42, 46, 47, 50, 51, 55, 57, 63, 66, 67, 69, 70, 76, 83, 89,
                90, 91, 93, 98, 99, 101, 103, 105, 124, 125, 126, 128, 130,
            };

            foreach (Match match in matches)
            {

                var index = Convert.ToInt32(match.Groups[1].Value);
                if(p.All(t => t != index)) continue;

                sb1.Append(match.Groups[1].Value);
                sb1.Append(".");
                if (match.Groups[3].Value.Length == 1)
                {
                    if (match.Groups[3].Value == "对" || match.Groups[3].Value == "错")
                    {
                        sb1.Append("[判断]");
                    }
                    else
                    {

                        sb1.Append("[单选]");
                    }
                }
                else
                {
                    sb1.Append("[多选]");
                }
                sb1.Append(match.Groups[2].Value);
                sb1.Append("答案：");
                sb1.Append(match.Groups[3].Value);

                if (match.Groups[5].Success)
                {
                    sb1.AppendLine();
                    sb1.Append("解析：");
                    sb1.Append(match.Groups[5].Value);
                }
                else
                {
                    sb1.Append("");
                }

                sb1.AppendLine();
                sb1.AppendLine();

                sb2.Append(match.Groups[1]);
                sb2.Append(".");
                sb2.Append(match.Groups[3].Value);

                if (match.Groups[5].Success)
                {
                    sb2.AppendLine();
                    sb2.Append(match.Groups[5].Value);
                }
                else
                {
                    sb2.Append("");
                }
                sb3.AppendLine();

                sb2.AppendLine();


                sb3.Append(match.Groups[1]);
                sb3.Append(".");
                if (match.Groups[5].Success)
                sb3.Append(match.Groups[5].Value);
                else
                {
                    sb3.Append("无");
                }
                sb3.AppendLine();


            }

            File.WriteAllText("C:\\Users\\loyse\\Desktop\\仅题目1.txt",sb1.ToString(),System.Text.Encoding.UTF8);
            File.WriteAllText("C:\\Users\\loyse\\Desktop\\仅答案1.txt", sb2.ToString(), System.Text.Encoding.UTF8);
            File.WriteAllText("C:\\Users\\loyse\\Desktop\\仅解析1.txt", sb3.ToString(), System.Text.Encoding.UTF8);
        }
    }
}