﻿namespace Vote.Project001.Core.Infrastructure
{
    public enum WebApiFailureEnum : int
    {
        VoteCountLimit=1002,
        VoteDeactivated = 1000,
        VoteMoreThanOnce = 1001,
    }

    public static class WebApiFailureMessage
    {
        public static readonly Dictionary<WebApiFailureEnum, string> WebApiFailureMessageDictionary =
            new Dictionary<WebApiFailureEnum, string>()
            {
                { WebApiFailureEnum.VoteCountLimit, "达到单个项目投票上限，单个项目可以投票 {0} 人。" },
                { WebApiFailureEnum.VoteMoreThanOnce, "你已经投过票，每个候选人只可以头一次票。" },
                { WebApiFailureEnum.VoteDeactivated , "投票已经结束。"}
            };
    }
}
