using System.Text;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using Vote.Project001.Core.Database;

namespace Vote.Project001.Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DatabaseContext.InitDatabase();

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddCors(opt =>
            {
                opt.AddDefaultPolicy(b =>
                {
                    b.AllowAnyHeader();
                    b.AllowAnyMethod();
                    b.AllowAnyOrigin();
                });
            });

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseSwagger((setup) =>
            {
                // setup.RouteTemplate
            });
            app.UseSwaggerUI();

            app.UseDefaultFiles();

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(),"wwwroot","static")),
                RequestPath = "/static",
                ContentTypeProvider = new FileExtensionContentTypeProvider()
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider =
                    new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "static")),
                RequestPath = "/static"
            });

            app.UseRouting();
            app.UseCors();

            app.UseAuthorization();

            app.MapControllerRoute("default", "api/{controller}/{action}/{id?}");

            app.Run();


        }
    }
}