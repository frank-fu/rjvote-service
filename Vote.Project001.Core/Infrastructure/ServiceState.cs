﻿namespace Vote.Project001.Core.Infrastructure
{
    public enum ServiceState
    {
        Idle = 0,
        Running = 1,
        Stopped = 2
    }
}
