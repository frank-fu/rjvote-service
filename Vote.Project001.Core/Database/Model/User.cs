﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vote.Project001.Core.Database.Model
{
    [Table("User")]
    public class User
    {
        [ Key, Column(TypeName = "NVARCHAR")]
        public string UserId { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Required]
        public string Name { get; set; }
    }
}
